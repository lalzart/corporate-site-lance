import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Hero from './components/Hero/Hero';
import Values from './components/Values/Values';
import People from './components/People/People';
class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Hero />
        <Values />
        <People />
      </div>
    );
  }
}

export default App;
