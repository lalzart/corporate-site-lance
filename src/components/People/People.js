import React, { Component } from 'react';
import './People.css';
import Akira from '../../assets/about/akira.jpg';
import Toshi from '../../assets/about/toshi.jpg';
import Kaka from '../../assets/about/kaka.jpg';
import Yosei from '../../assets/about/yosei.jpg';
import Title from '../Title/Title';

class People extends Component {

  constructor(props) {
    super(props);
    this.handleMouseHover = this.handleMouseHover.bind(this);
    this.state = {
      hovering: false,
    };
  }

  handleMouseHover() {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return {
      hover: !state.hover,
    };
  }


  render() {
    return (
      <React.Fragment>
      <Title description="People"/>
      <div className="container">
        <div className="flexRow">
          <div className="person">
            <div 
              onMouseEnter={this.handleMouseHover}
              onMouseLeave={this.handleMouseHover}
              className="toshi"> 
              {  this.state.hover &&
                <div className="toshiInfo">Toshi is the CEO and founder of Zenport. He holds undergrad and master degree from the University of Tokyo.</div>
              }
              <img src={Toshi} alt="" className="large"/>
            </div>
            <h4 className="name">Toshihiro KASEDA</h4>
            <h6 className="role">CEO, Founder</h6>
          </div>
          <div className="person">
            <img src={Kaka} alt="" className="medium"/>
            <h4 className="name">Kaka Huynh</h4>
            <h6 className="role">Lead Front-end Engineer</h6>
          </div>
          <div className="person personSmall">
            <img src={Akira} alt="" className="small"/>
            <h4 className="name">Akira Ishikawa</h4>
            <h6 className="role">Business Development</h6>
          </div>
          <div className="person">
            <img src={Yosei} alt="" className="large"/>
            <h4 className="name">Yosei Ito</h4>
            <h6 className="role">Business Development</h6>
          </div>
        </div>
        <div className="flexRow flexCenter">
          <div className="person ">
            <img src={Akira} alt="" className="large"/>
            <h4 className="name">Toshihiro KASEDA</h4>
            <h6 className="role">CEO, Founder</h6>
          </div>
          <div className="person">
            <img src={Akira} alt="" className="medium"/>
            <h4 className="name">Kaka Huynh</h4>
            <h6 className="role">Lead Front-end Engineer</h6>
          </div>
          <div className="person personSmall">
            <img src={Akira} alt="" className="small"/>
            <h4 className="name">Akira Ishikawa</h4>
            <h6 className="role">Business Development</h6>
          </div>
          <div className="person">
            <img src={Akira} alt="" className="large"/>
            <h4 className="name">Yosei Ito</h4>
            <h6 className="role">Business Development</h6>
          </div>
        </div>
        <div className="flexRow">
          <div className="person ">
            <img src={Akira} alt="" className="large"/>
            <h4 className="name">Toshihiro KASEDA</h4>
            <h6 className="role">CEO, Founder</h6>
          </div>
          <div className="person">
            <img src={Akira} alt="" className="medium"/>
            <h4 className="name">Kaka Huynh</h4>
            <h6 className="role">Lead Front-end Engineer</h6>
          </div>
          <div className="person personSmall">
            <img src={Akira} alt="" className="small"/>
            <h4 className="name">Akira Ishikawa</h4>
            <h6 className="role">Business Development</h6>
          </div>
          <div className="person">
            <img src={Akira} alt="" className="large"/>
            <h4 className="name">Yosei Ito</h4>
            <h6 className="role">Business Development</h6>
          </div>
        </div>

      </div>
      </React.Fragment>
    )
  }
}

export default People;
