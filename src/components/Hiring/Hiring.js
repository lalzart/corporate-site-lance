import React from 'react'
import './Hiring.css'
import Team from '../../assets/career/all-1.jpg'
export default function Hiring() {
  return (
    <div className="hiringContainer">
      <img src="" alt="" className="globe"/>
      <img src={Team} alt="" className="team"/>
      <div className="hiringInfo"></div>
    </div>
  )
}
